var map = L.map('main_map').setView([4.5030293, -74.1303945], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreeMap</a> contributors'
}).addTo(map);

L.marker([4.5228244, -74.1240342]).addTo(map);
L.marker([4.5220049, -74.1235917]).addTo(map);
L.marker([4.522898, -74.1235863]).addTo(map);






